package org.ashish;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.SystemPropertiesCredentialsProvider;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.Bucket;
import com.amazonaws.services.s3.model.GetBucketLocationRequest;
import com.amazonaws.services.s3.model.ObjectListing;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.S3ObjectSummary;

public class S3Operations {

	@SuppressWarnings("deprecation")
	private final static  AmazonS3 getS3Client(){
		AWSCredentials credentials = null;
		try {
			credentials = new SystemPropertiesCredentialsProvider().getCredentials();
		} catch (Exception e) {
			throw new AmazonClientException("Cannot load the credentials ",e);
		}
		AmazonS3 s3client = new AmazonS3Client(credentials);
		Region usWest2 = Region.getRegion(Regions.US_WEST_2);
		s3client.setRegion(usWest2);
		return s3client;
	}


	protected void deleteAllBuckets(){
		final AmazonS3 s3 = getS3Client();
		try {
			for (Bucket bucket : s3.listBuckets()) {

				System.out.println(" - removing objects from bucket");
				ObjectListing object_listing = s3.listObjects(bucket.getName());
				while (true) {
					for (Iterator<?> iterator =
							object_listing.getObjectSummaries().iterator();
							iterator.hasNext();) {
						S3ObjectSummary summary = (S3ObjectSummary)iterator.next();
						s3.deleteObject(bucket.getName(), summary.getKey());
					}

					// more object_listing to retrieve?
					if (object_listing.isTruncated()) {
						object_listing = s3.listNextBatchOfObjects(object_listing);
					} else {
						break;
					}
				};
			}
		} catch (AmazonServiceException e) {
			System.err.println(e.getErrorMessage());
			System.exit(1);
		}


	}

	protected void uploadProfile() throws Exception{
		String bucketName = "cloudApp.ashish.resume.bucket" + UUID.randomUUID();
		String key = "profileMetaKey";
		uploadFiles("profile",bucketName, key);
	}

	protected void uploadResume() throws Exception{
		String bucketName = "cloudApp.ashish.resume.bucket" + UUID.randomUUID();
		String key = "resumeMetaKey";
		uploadFiles("resume",bucketName,key);
	}

	public void uploadFiles(String bucketType, String bucketName, String key) throws IOException {
		final AmazonS3 s3client = getS3Client();
		//if bucket exist, clean up and add new 
		// else create new and add

		try {
			//we can use s3.bucketList as well . but it's ok
			if(!(s3client.doesBucketExist(bucketName))){
				System.out.println("Bucket Doesn't exist");
				System.out.println("Creating bucket " + bucketName + "\n");
				s3client.createBucket(bucketName);
				String bucketLocation = s3client.getBucketLocation(new GetBucketLocationRequest(bucketName));
				System.out.println("Profile bucket location = " + bucketLocation);
			}
			else{
				System.out.println(" ProfileBucket Exist. Cleaning up current data");
			}

			ClassLoader classLoader = getClass().getClassLoader();
			for (String fileName : getResourceFiles(bucketType)) {
				File file = new File(classLoader.getResource(bucketType+"//"+fileName).getFile());
				s3client.putObject(new PutObjectRequest(bucketName, key, file));
			}
		} catch (AmazonServiceException ase) {
			System.out.println("Error Message:    " + ase.getMessage());
			System.out.println("HTTP Status Code: " + ase.getStatusCode());
			System.out.println("AWS Error Code:   " + ase.getErrorCode());
			System.out.println("Error Type:       " + ase.getErrorType());
			System.out.println("Request ID:       " + ase.getRequestId());
		} catch (AmazonClientException ace) {
			System.out.println("Error Message: " + ace.getMessage());
		}

	}

	private List<String> getResourceFiles(String path) throws IOException {
		List<String> filenames = new ArrayList<>();
		try(
				InputStream in = getResourceAsStream( path );
				BufferedReader br = new BufferedReader( new InputStreamReader( in ) ) ) {
			String resource;
			while( (resource = br.readLine()) != null ) {
				filenames.add( resource );
			}
		}

		return filenames;
	}

	private InputStream getResourceAsStream( String resource ) {
		final InputStream in = getContextClassLoader().getResourceAsStream( resource );

		return in == null ? getClass().getResourceAsStream( resource ) : in;
	}

	private ClassLoader getContextClassLoader() {
		return Thread.currentThread().getContextClassLoader();
	}

	//TODO
	public void downloadFile(String fileType, String fileName, Map metadata){
		
		//if file type is profile , download from profile  else from resume 
		// map is future imp
		/*
		 * Download an object - When you download an object, you get all of
		 * the object's metadata and a stream from which to read the contents.
		 * It's important to read the contents of the stream as quickly as
		 * possibly since the data is streamed directly from Amazon S3 and your
		 * network connection will remain open until you read all the data or
		 * close the input stream.
		 *
		 * GetObjectRequest also supports several other options, including
		 * conditional downloading of objects based on modification times,
		 * ETags, and selectively downloading a range of an object.
		 */
		/*System.out.println("Downloading an object");
		S3Object object = s3client.getObject(new GetObjectRequest(bucketName, key));
		System.out.println("Content-Type: "  + object.getObjectMetadata().getContentType());
		displayTextInputStream(object.getObjectContent());

		System.out.println("Listing objects");
		ObjectListing objectListing = s3client.listObjects(new ListObjectsRequest().withBucketName(bucketName).withPrefix("My"));
		for (S3ObjectSummary objectSummary : objectListing.getObjectSummaries()) {
			System.out.println(" - " + objectSummary.getKey() + "  " +
					"(size = " + objectSummary.getSize() + ")");
		}*/
	}

	//TODO
	private void displayTextInputStream(InputStream input) throws IOException {
		BufferedReader reader = new BufferedReader(new InputStreamReader(input));
		while (true) {
			String line = reader.readLine();
			if (line == null) break;

			System.out.println("    " + line);
		}
		System.out.println();
	}

}